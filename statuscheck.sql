-- Check SAP/Sybase ASE server & database status
--
-- Original code from
--
-- 1) Dataserver Health Check - Version 1.0
-- Intial Drafted by - sybanva for SYBASETEAM.COM - Date - 19th Feb 2010a
--
-- 2) http://www.sybaseteam.com/sybase-sql-script-find-out-user-database-data-t-948.html#pid1357

set nocount on
go
set proc_return_status off
go

-- Server version etc

select @@servername as "Server", @@boottime as "Started", getdate() as "Current time"
select substring(@@version, 1, 135)
go

-- sp_monitorconfig -details

create table #monitor_results (
Name varchar(30),
Config_val int,
System_val int,
Total_val int,
Num_free int,
Num_active int,
Pct_act char(6),
Max_Used int,
Reuse_cnt int,
Date date,
Instance_Name varchar)
go

-- Collect only what you want ...

-- sp_monitorconfig "max memory", "#monitor_results"
-- go
-- sp_monitorconfig "number of locks", "#monitor_results"
-- go
-- sp_monitorconfig "number of open indexes", "#monitor_results"
-- go
-- sp_monitorconfig "number of open objects", "#monitor_results"
-- go
-- sp_monitorconfig "number of user connection", "#monitor_results"
-- go
-- sp_monitorconfig "procedure cache size", "#monitor_results"
-- go

-- ... Or get all metrics in one go.

sp_monitorconfig "all", "#monitor_results"
go

print ""
print "========== Usage statistics:"
print "" 
select * from #monitor_results order by Name
go

--
-- db engines, process status, Long running, blocking & zombies
--

print ""
print "========== DB engines:"
print ""
select engine "Engine No",osprocid "OC Proc ID",status "STATUS",starttime "START TIME" from sysengines
go

print ""
print "========== Process Status:"
print ""
select spid,cmd,cpu,physical_io,ipaddr,loggedindatetime,hostprocess from sysprocesses where physical_io>0 or cpu >0
go

print ""
print "========== Long running transactions:"
print ""
If exists (select count(1) from syslogshold where spid <> 0)
print "NONE"
else
select dbid "Database ID",spid SPID,starttime "START TIME",name NAME from syslogshold where spid <> 0
go

print ""
print "========== Blocking processes:"
print ""
if not exists (select count(1) from sysprocesses where blocked > 0)
select spid SPID,cmd COMMAND,cpu "CPU Usage",physical_io "PHYSICAL IO",ipaddr "IP ADDRESS",loggedindatetime "LOGGED IN DATE",hostprocess "Host PROCESS ID"  from sysprocesses where blocked > 0
else
print "NONE"
go

print ""
print "========== Zombie SPIDs:"
print ""
if exists(select spid from master..syslogshold slh where slh.spid <> 0 and slh.spid not in(select spid from master..sysprocesses))
select spid from master..syslogshold slh where slh.spid <> 0 and slh.spid not in(select spid from master..sysprocesses)
else
print "NONE"
go

-- 
-- Individual DB status
--

print ""
print "========== DB status:"
print ""
declare @pagesize int
select @pagesize=(select @@maxpagesize)
SELECT "Database Name" = CONVERT(char(20), db_name(D.dbid)),
"Data Size" = STR(SUM(CASE WHEN U.segmap != 4 THEN U.size*@pagesize/1048576 END),10,1),
"Used Data" = STR(SUM(CASE WHEN U.segmap != 4 THEN size - curunreservedpgs(U.dbid, U.lstart, U.unreservedpgs)END)*@pagesize/1048576,10,1),
"Data Full%" = STR(100 * (1 - 1.0 * SUM(CASE WHEN U.segmap != 4 THEN curunreservedpgs(U.dbid, U.lstart, U.unreservedpgs) END)/SUM(CASE WHEN U.segmap != 4 THEN U.size END)),9,1) + "%",
"Log Size" = STR(SUM(CASE WHEN U.segmap = 4 THEN U.size*@pagesize/1048576 END),10,1),
"Free Log" = STR(lct_admin("logsegment_freepages",D.dbid)*@pagesize/1048576,10,1),
"Log Full%" = STR(100 * (1 - 1.0 * lct_admin("logsegment_freepages",D.dbid) /
SUM(CASE WHEN U.segmap = 4 THEN U.size END)),8,1) + "%",
D.dumptrdate as "Latest dump"
FROM master..sysdatabases D,
master..sysusages U
WHERE U.dbid = D.dbid
AND ((D.dbid != 3) AND (D.dbid < 31513) AND (D.status != 256))
GROUP BY D.dbid
ORDER BY db_name(D.dbid)
go

print ""
print "========== Suspect/offline DBs:"
print ""
if exists(select name from sysdatabases where status in (-32768,64,256,32))
select name, status from sysdatabases where status in (-32768,64,256,32)
else
print "NONE"
go

